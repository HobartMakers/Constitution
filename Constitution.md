**SCHEDULE 1 - Model rules for an association**

**1. Name of association**

The name of the association is as follows:

Hobart Makers

**2. Interpretation**

In these rules, unless the context otherwise requires – **accounting records** has the same meaning as in the Act;

**Act** means the Associations Incorporation Act 1964 ;
 **annual general meeting** means an annual general meeting of the Association

held under rule 13 ;

**Association** means the association referred to in rule 1 ;

**association** has the same meaning as in the Act;

**auditor** means the person appointed as the auditor of the Association under rule 10 ;

**authorised deposit-taking institution** means a body corporate that is an authorised deposit-taking institution for the purposes of the Banking Act 1959 of the Commonwealth;

**basic objects of the Association** means the objects and purposes of the Association as stated in an application under section 7 of the Act for the incorporation of the Association;

**committee** means the committee of management referred to in rule 23 ; **financial year** has the same meaning as in the Act;

**general meeting** means –
 (a) an annual general meeting; or

(b) a special general meeting;

**officer of the Association** means a person elected as an officer of the Association at an annual general meeting or appointed as an officer of the Association under rule 24(5) ;

**ordinary business of an annual general meeting** means the business specified in rule 13(5) ;

**ordinary committee member** means a member of the committee other than an officer of the Association;

Regulation 3

**public officer** means the person who is, under section 14 of the Act, the public officer of the Association;

**special committee meeting** means a meeting of the committee that is convened under rule 28(2) by the president or any 4 of the members of the committee;

**special general meeting** means a special general meeting of the Association convened under rule 14 ;

**special resolution** has the same meaning as in the Act.

**3. Association's office**

The office of the Association is to be at the following place or at any other place the committee determines:

Enterprize, Level 5/24 Davey St, Hobart TAS 7000

**4. Objects and purposes of Association**

The objects and purposes of the Association consist of the basic objects of the Association and the following objects and purposes:
 (a) the purchase, taking on lease or in exchange, hire or other acquisition of any real or personal property necessary or convenient for any of the objects or purposes of the Association;

(b) the purchase, sale or supply of, or other dealing in, goods;

(c) the construction, maintenance or alteration of any building or works necessary or convenient for any of the objects or purposes of the Association;

(d) the acceptance of a gift for any of the objects or purposes of the Association;

(e) the taking of any step the committee, or the members of the Association at a general meeting, determine expedient for the purpose of procuring contributions to the funds of the Association;

(f) the printing or publication of any newspaper, periodical, book, leaflet or other document the committee, or the members of the Association at a general meeting, determine desirable for the promotion of any of the objects or purposes of the Association;

(g) the borrowing and raising of money in any manner and on terms – (i) the committee thinks fit; or

(ii) approved or directed by resolution passed at a general meeting;

(h) subject to the provisions of the Trustee Act 1898 , the investment, in any manner the committee determines, of any money of the Association not immediately required for any of the objects or purposes of the Association;

(i) the making of a gift, subscription or donation to any of the funds, authorities or institutions to which section 78A of the Income Tax Assessment Act 1936 of the Commonwealth relates;

(j) the establishment and support, or aiding in the establishment and support, of associations, institutions, funds, trusts, schemes or conveniences calculated to benefit employees or past employees of the Association and their dependants, and the granting of pensions, allowances or other benefits to employees or past employees of the Association and their dependants, and the making of payments towards insurance in relation to any of those purposes;

(k) the establishment and support, or aiding in the establishment or support, of any other association formed for any of the basic objects of the Association;

(l) the purchase, or acquisition, and the undertaking of all or part of the property, assets, liabilities or engagements of any association with which the Association is amalgamated in accordance with the provisions of the Act and the rules of the Association;

(m) the doing of any lawful thing incidental or conducive to the attainment of the objects or purposes of the Association.

**5. Membership of Association**

(1) A person who is nominated and approved for membership in accordance with this rule is eligible to be a member of the Association on payment of the annual subscription specified in rule 32 .

(2) A person who is not a member of the Association at the time of the incorporation of the Association is not to be admitted as a member of the Association unless –
 (a) the person is nominated for membership in accordance with subrule (3) ; and

 (b) the person is approved for membership by the committee.

(3) A nomination of a person for membership is to be –
 (a) made in writing and signed by 2 members of the Association; and

 (b) accompanied by the written consent of the person nominated; and

 (c) lodged with the public officer.

(4) The consent referred to in subrule (3)(b) may be endorsed on the nomination.

(5) As soon as practicable after the receipt of a nomination, the public officer is to refer the nomination to the committee.

(6) If a nomination is approved by the committee, the public officer is to – (a) notify the nominee, in writing, that the nominee has been approved for membership of the Association; and

(b) on receipt of the amount payable by the nominee as the first annual subscription, enter the nominee's name in the register of members.

(7) A member of the Association may resign by serving on the public officer a written notice of resignation.

(8) On receipt of a notice from a member of the Association under
 subrule (7) , the public officer is to remove the name of the member from the register of members.

(9) A person –
 (a) becomes a member of the Association when his or her name is entered in the register of members; and

(b) ceases to be a member of the Association when his or her name is removed from the register of members under subrule (8) or rule 32(4).

(10) The public officer is to maintain, or establish and maintain, a register of members containing –
 (a) the name of each member of the Association and the date on which he or she became a member; and

(b) the member's postal or residential address or address of business or employment; and

(c) an email address, if any, that the member has nominated as the email address to which notices from the Association may be sent; and

(d) the name of each person who has ceased to be a member of the Association and the date on which the person ceased to be a member of the Association.

**6. Liability of members**

(1) Any right, privilege or obligation of a person as a member of the Association –
 (a) is not capable of being transferred to another person; and

(b) terminates when the person ceases to be a member of the Association.

(2) If the Association is wound up, each person who was, immediately before the Association is wound up, a member of the Association, and each person who was a member of the Association within the period of 12 months immediately preceding the commencement of the winding-up, is liable to contribute –

(a) to the assets of the Association for payment of the liabilities of the Association; and

(b) for the costs, charges and expenses of the winding-up; and
 (c) for the adjustment of the rights of the contributors among themselves. (3) Any liability under subrule (2) is not to exceed $1.

(4) Despite subrule (2) , a former member of the Association is not liable to contribute under that subrule in respect of any liability of the Association incurred after he or she ceased to be a member.

**7. Income and property of Association**

(1) The income and property of the Association is to be applied solely towards the promotion of the objects and purposes of the Association.

(2) No portion of the income or property of the Association is to be paid or transferred to any member of the Association unless the payment or transfer is made in accordance with this rule.

(3) The Association may –
 (a) pay a person or member of the Association –
 (i) remuneration in return for services rendered to the Association, or for goods supplied to the Association, in the ordinary course of business of the person or member; or

(ii) remuneration that constitutes a reimbursement for out-of-pocket expenses incurred by the person or member for any of the objects or purposes of the Association; or

(iii) interest at a rate not exceeding 7.25% on money lent to the Association by the person or member; or

(iv) a reasonable amount by way of rent for premises, or a part of premises, let to the Association by the person or member; and

(b) pay a member of the committee remuneration in return for carrying out the functions of a member of the committee; and

(c) pay a member of a subcommittee remuneration in return for carrying out the functions of a member of the subcommittee; and

(d) if so requested by or on behalf of any other association, organisation or body, appoint or nominate a member of the Association to an office in that other association, organisation or body.

(4) Despite subrule (3)(a) , (b) and (c) , the Association is not to pay a person any amount under that subrule unless the Association or committee has first approved that payment.

(5) Despite subrule (3)(d) , the Association is not to appoint or nominate a member of the Association under that subrule to an office in respect of which remuneration is payable unless the Association or committee has first approved –

(a) that appointment or nomination; and

(b) the receipt of that remuneration by that member.

**8. Accounts of receipts and expenditure**

(1) True accounts are to be kept of the following:
 (a) each receipt or payment of money by the Association and the matter in respect of which the money was received or paid;

(b) each asset or liability of the Association.

(2) The accounts are to be open to inspection by the members of the Association at any reasonable time, and in any reasonable manner, determined by the committee.

(3) The treasurer of the Association is to keep all accounting books, and general records and records of receipts and payments, connected with the business of the Association in the form and manner the committee determines.

(4) The accounts, books and records are to be kept at the Association's office or at any other place the committee determines.

**9. Banking and finance**

(1) On behalf of the Association, the treasurer of the Association is to – (a) receive any money paid to the Association; and

(b) immediately after receiving the money, issue an official receipt in respect of the money; and

(c) cause the money to be paid into the account opened under subrule (2) as soon as practicable after it is received.

(2) The committee is to open with an authorised deposit-taking institution an account in the name of the Association.

(3) The committee may –
 (a) receive from an authorised deposit-taking institution a cheque drawn by the Association on any of the Association's accounts with the authorised deposit-taking institution; and

(b) release or indemnify the authorised deposit-taking institution from or against any claim, or action or other proceeding, arising directly or indirectly out of the drawing of that cheque.

(4) Except with the authority of the committee, a payment of an amount exceeding $20 is not to be made from the funds of the Association other than –
 (a) by cheque drawn on the Association's account; or

(b) by the electronic transfer of funds from the Association's account to another account at an authorised deposit-taking institution.

(5) The committee may provide the treasurer of the Association with an amount of money to meet urgent expenditure, subject to any conditions the committee may impose in relation to the expenditure.

(6) A cheque is not to be drawn on the Association's account, and an amount is not to be electronically transferred from the Association's account to another account at an authorised deposit-taking institution, except for the purpose of making a payment that has been authorised by the committee.

(7) A cheque, draft, bill of exchange, promissory note or other negotiable instrument is to be –
 (a) signed by the treasurer of the Association or, in the treasurer's absence, by any other member, or members, of the committee the committee nominates for that purpose; and

(b) countersigned by the public officer.

(8) An electronic transfer of an amount from the Association's account to another account at an authorised deposit-taking institution –
 (a) may only be authorised by the treasurer of the Association or, in the treasurer's absence, by any other member, or members, of the committee the committee nominates for that purpose; and

(b) may only be authorised by a person referred to in paragraph (a) if the authorisation has been approved by the public officer.

**10. Auditor**

(1) At each annual general meeting, the members of the Association present at the meeting are to appoint a person as the auditor of the Association.

(2) If an auditor is not appointed at an annual general meeting under subrule (1) , the committee is to appoint a person as the auditor of the Association as soon as practicable after that annual general meeting.

(3) The auditor is to hold office until the next annual general meeting and is eligible for re-appointment.

(4) The first auditor –
 (a) may be appointed by the committee before the first annual general meeting; and

(b) if so appointed, holds office until the end of the first annual general meeting unless earlier removed by a resolution of the members of the Association at a general meeting.

(5) If the first auditor is appointed by the committee under subrule (4)(a) and subsequently removed at a general meeting under subrule (4)(b) , the members of the Association, at that general meeting, may appoint an auditor to hold office until the end of the first annual general meeting.

(6) Except as provided in subrule (4)(b) , the auditor may only be removed from office by special resolution.

(7) If a casual vacancy occurs in the office of auditor, the committee is to appoint a person to fill the vacancy until the end of the next annual general meeting.

**11. Audit of accounts**

(1) The auditor is to audit the financial affairs of the Association at least once in each financial year of the Association.

(2) The auditor, after auditing the financial affairs of the Association for a particular financial year of the Association, is to –
 (a) certify as to the correctness of the accounts of the Association; and

(b) at the next annual general meeting, provide a written report to the members of the Association who are present at that meeting.

(3) In the report and in certifying to the accounts, the auditor is to – (a) specify the information, if any, that he or she has required under subrule (5)(b) and obtained; and

(b) state whether, in his or her opinion, the accounts exhibit a true and correct view of the financial position of the Association according to the information at his or her disposal; and

(c) state whether the rules relating to the administration of the funds of the Association have been observed.

(4) The public officer is to deliver to the auditor a list of all the accounting records, books and accounts of the Association.

(5) The auditor may –
 (a) have access to the accounting records, books and accounts of the Association; and

(b) require from any employee of, or person who has acted on behalf of, the Association any information the auditor considers necessary for the performance of his or her duties; and

(c) employ any person to assist in auditing the financial affairs of the Association; and

(d) examine any member of the committee, or any employee of, or person who has acted on behalf of, the Association, in relation to the accounting records, books and accounts of the Association.

**12. Exemptions under the Act**

(1) For any financial year that the Association is exempt from the requirement to be audited by virtue of section 24(1B) or (1C) of the Act – (a) an auditor is not required to be appointed for that financial year under rule 10 unless the Association elects to have the financial affairs of the Association for that financial year audited in accordance with the Act and these rules; and

(b) if an auditor is not appointed for a financial year by virtue of paragraph (a) –
 (i) rules 10 and 11 do not apply in respect of the Association for that financial year; and

(ii) rule 13(5)(b) , to the extent that it relates to an auditor, does not apply in respect of the annual general meeting held by the Association in respect of that financial year; and

(iii) rule 13(5)(d) does not apply in respect of the annual general meeting held by the Association in respect of that financial year.

(2) For any financial year that the Association is exempt from the requirement to provide an annual return by virtue of section 24(1B) of the Act, the committee must provide, as part of the ordinary business of the annual general meeting for that financial year, a copy of the annual financial report given under the Australian Charities and Not-for-profits Commission Act 2012 of the Commonwealth in respect of that financial year.

**13. Annual general meeting**

(1) The Association is to hold an annual general meeting each year.

(2) An annual general meeting is to be held on any day (being not later than 3 months after the end of the financial year of the Association) the committee determines.

(3) An annual general meeting is to be in addition to any other general meeting that may be held in the same year.

(4) The notice convening an annual general meeting is to specify the purpose of the meeting.

(5) The ordinary business of an annual general meeting is to be as follows:

(a) to confirm the minutes of the last preceding annual general meeting and of any general meeting held since that meeting;

(b) to receive from the committee, auditor, employees and other persons acting on behalf of the Association reports on the transactions of the Association during the last preceding financial year of the Association;

(c) to elect the officers of the Association and the ordinary committee members;

(d) to appoint the auditor and determine his or her remuneration;

(e) to determine the remuneration of employees and other persons acting on behalf of the Association.

(6) An annual general meeting may transact business of which notice is given in accordance with rule 15 .

(7) Minutes of proceedings of an annual general meeting are to be kept, in the minute book of the Association, by the public officer or, in the absence from the meeting of the public officer, by an officer of the Association who is nominated by the chairperson of the meeting.

**14. Special general meetings**

(1) The committee may convene a special general meeting of the Association at any time.

(2) The committee, on the requisition in writing of at least 10 members of the Association, is to convene a special general meeting of the Association.

(3) A requisition for a special general meeting – (a) is to state the objects of the meeting; and

(b) is to be signed by each of the requisitionists; and
 (c) is to be deposited at the office of the Association; and

(d) may consist of several documents, each signed by one or more of the requisitionists.

(4) If the committee does not cause a special general meeting to be held within 21 days after the day on which a requisition is deposited at the office of the Association, any one or more of the requisitionists may convene the meeting within 3 months after the day on which the requisition is deposited at the office of the Association.

(5) A special general meeting convened by requisitionists is to be convened in the same manner, as nearly as practicable, as the manner in which a special general meeting would be convened by the committee.

(6) All reasonable expenses incurred by requisitionists in convening a special general meeting are to be refunded by the Association.

**15. Notices of general meetings**

(1) At least 14 days before the day on which a general meeting of the Association is to be held, the public officer is to publish a notice specifying – (a) the place, day and time at which the meeting is to be held; and

(b) the nature of the business that is to be transacted at the meeting.

(2) A notice is published for the purposes of subrule (1) if the notice – (a) is contained in an advertisement appearing in at least one newspaper circulating in Tasmania; or

(b) appears on a website, or at an electronic address, of the Association; or

(c) is sent to each member of the Association at –
 (i) the member's postal or residential address or address of business or employment; or

(ii) an email address that the member has nominated as the email address to which notices from the Association may be sent; or

(d) is given by another means, determined by the public officer, that is reasonably likely to ensure that the members of the Association will be notified of the notice.

**16. Business and quorum at general meetings**

(1) All business transacted at a general meeting, other than the ordinary business of an annual general meeting, is special business.

(2) Business is not to be transacted at a general meeting unless a quorum of members of the Association entitled to vote is present at the time when the meeting considers that business.

(3) A quorum for the transaction of the business of a general meeting is 5 members of the Association entitled to vote.

(4) If a quorum is not present within one hour after the time appointed for the commencement of a general meeting, the meeting –
 (a) if convened on the requisition of members of the Association, is dissolved; or

(b) if convened by the committee, is to be adjourned to the same day in the next week at the same time and –
 (i) at the same place; or

(ii) at any other place specified by the chairperson – (A) at the time of the adjournment; or

(B) by notice in a manner determined by the chairperson.

(5) If at an adjourned general meeting a quorum is not present within one hour after the time appointed for the commencement of the meeting, the meeting is dissolved.

**17. Chairperson at general meetings**

At each general meeting of the Association, the chairperson is to be – (a) the president; or

(b) in the absence of the president, the senior vice-president; or

(c) in the absence of the president and the senior vice-president, the other vice-president; or

(d) in the absence of the president and both vice-presidents, a member of the Association elected to preside as chairperson by the members of the Association present and entitled to vote at the general meeting.

**18. Adjournment of general meetings**

(1) The chairperson of a general meeting at which a quorum is present may adjourn the meeting with the consent of the members of the Association who are present and entitled to vote at the meeting, but no business is to be transacted at an adjourned meeting other than the business left unfinished at the meeting at which the adjournment took place.

(2) If a meeting is adjourned for 14 days or more, notice of the adjourned meeting is to be given in the same manner as the notice of the original meeting.

(3) If a meeting is adjourned for less than 14 days, it is not necessary to give any notice of the adjournment or of the business to be transacted at the adjourned meeting.

**19. Determination of questions arising at general meetings**

(1) A question arising at a general meeting of the Association is to be determined on a show of hands.

(2) A declaration by the chairperson that a resolution has, on a show of hands, been lost or carried, or been carried unanimously or carried by a particular majority, together with an entry to that effect in the minute book of the Association, is evidence of that fact unless a poll is demanded on or before that declaration.

**20. Votes**

(1) On any question arising at a general meeting of the Association, a member of the Association (including the chairperson) has one vote only.

(2) All votes are to be given personally.

(3) Despite subrule (1) , in the case of an equality of votes, the chairperson has a second or casting vote.

**21. Taking of poll**

If at a general meeting a poll on any question is demanded –
 (a) the poll is to be taken at that meeting in the manner that the chairperson determines; and

(b) the result of the poll is taken to be the resolution of the meeting on that question.

**22. When poll to be taken**

(1) A poll that is demanded on the election of a chairperson, or on a question of adjournment, is to be taken immediately.

(2) A poll that is demanded on any other question is to be taken at any time before the close of the meeting as the chairperson determines.

**23. Affairs of Association to be managed by a committee**

(1) The affairs of the Association are to be managed by a committee of management constituted as provided in rule 25 .

(2) The committee –
 (a) is to control and manage the business and affairs of the Association; and

(b) may exercise all the powers and perform all the functions of the Association, other than those powers and functions that are required by these rules to be exercised and performed by members of the Association at a general meeting; and

(c) has power to do anything that appears to the committee to be essential for the proper management of the business and affairs of the Association.

**24. Officers of the Association**

(1) The officers of the Association are as follows:

1. (a)  the president;
2. (b)  two vice-presidents;
3. (c)  the treasurer;
4. (d)  the secretary.

1. (2)  One of the vice-presidents is to be known as the senior vice-president.
2. (3)  Subject to subrule (5) , the officers of the Association are to be elected in

accordance with rule 26 .

(4) Each officer of the Association is to hold office until the end of the next annual general meeting after that at which he or she is elected and is eligible for re-election.

(5) If a casual vacancy in an office referred to in subrule (1) occurs, the committee may appoint one of its members to fill the vacancy until the end of the next annual general meeting after the appointment.

(6) If an office referred to in subrule (1) is not filled at an annual general meeting, there is taken to be a casual vacancy in the office.

**25. Constitution of the committee**

(1) The committee consists of –
 (a) the officers of the Association; and

(b) 2 other members elected at the annual general meeting or appointed in accordance with this rule.

(2) An ordinary committee member is to hold office until the end of the next annual general meeting after that at which he or she is elected and is eligible for re-election.

(3) If a casual vacancy occurs in the office of an ordinary committee member, the committee may appoint a member of the Association to fill the vacancy until the end of the next annual general meeting after the appointment.

(4) If an office of an ordinary committee member is not filled at an annual general meeting, there is taken to be a casual vacancy in the office.

**26. Election of numbers of committee**

(1) A nomination of a candidate for election as an officer of the Association, or as an ordinary committee member, is to be –
 (a) made in writing, signed by 2 members of the Association and accompanied by the written consent of the candidate (which may be endorsed on the nomination); and

(b) delivered to the public officer at least 10 days before the day on which the annual general meeting is to be held.

(2) If insufficient nominations are received to fill all vacancies on the committee –
 (a) the candidates nominated are taken to be elected; and

(b) further nominations are to be received at the annual general meeting.

(3) If the number of nominations received is equal to the number of vacancies on the committee to be filled, the persons nominated are taken to be elected.

(4) If the number of nominations received exceeds the number of vacancies on the committee to be filled, a ballot is to be held.

(5) If the number of further nominations received at the annual general meeting exceeds the number of remaining vacancies on the committee to be filled, a ballot is to be held in relation to those further nominations.

(6) The ballot for the election of officers of the Association and ordinary committee members is to be conducted at the annual general meeting in the manner determined by the committee.

**27. Vacation of office**

For the purpose of these rules, the office of an officer of the Association, or of an ordinary committee member, becomes casually vacant if the officer or committee member –
 (a) dies; or

(b) becomes bankrupt, applies to take the benefit of any law for the relief of bankrupt or insolvent debtors, compounds with his or her creditors or makes an assignment of his or her remuneration or estate for their benefit; or

(c) becomes a represented person within the meaning of the Guardianship and Administration Act 1995 ; or

(d) resigns office in writing addressed to the committee; or (e) ceases to be ordinarily resident in Tasmania; or

(f) is absent from 3 consecutive meetings of the committee without the permission of the other members of the committee; or

(g) ceases to be a member of the Association; or

(h) fails to pay, within 14 days after receiving a notice in writing signed by the public officer stating that the officer or committee member has failed to pay one or more amounts of annual subscriptions, all such amounts due and payable by the officer or member.

**28. Meetings of the committee**

(1) The committee is to meet at least once in each month at any place and time the committee determines.

(2) A meeting of the committee, other than a meeting referred to in subrule (1) , may be convened by the president or any 4 of the members of the committee.

(3) Written notice of any special committee meeting is to be served on members of the committee and is to specify the general nature of the business to be transacted.

(4) A special committee meeting may only transact business of which notice is given in accordance with subrule (3) .

(5) A quorum for the transaction of the business of a meeting of the committee is 4 members of the committee.

(6) Business is not to be transacted at a meeting of the committee unless a quorum is present.

(7) If a quorum is not present within half an hour after the time appointed for the commencement of –
 (a) a meeting of the committee (other than a special committee meeting), the meeting is to be adjourned to the same day in the next week at the same time and at the same place; or

(b) a special committee meeting, the meeting is dissolved.

(8) At each meeting of the committee, the chairperson is to be – (a) the president; or

(b) in the absence of the president, the senior vice-president; or

(c) in the absence of the president and the senior vice-president, the other vice-president; or

(d) in the absence of the president and both vice-presidents, a member of the committee elected to preside as chairperson by the members of the committee present at the meeting.

(9) Any question arising at a meeting of the committee is to be determined – (a) on a show of hands; or

(b) if demanded by a member, by a poll taken at that meeting in the manner the chairperson determines.

(10) On any question arising at a meeting of the committee, a member of the committee (including the chairperson) has one vote only.

(11) Despite subrule (10) , in the case of an equality of votes, the chairperson has a second or casting vote.

(12) Written notice of each committee meeting is to be served on each member of the committee by –
 (a) giving it to the member during business hours before the day on which the meeting is to be held; or

(b) leaving it, during business hours before the day on which the meeting is to be held, at the member's postal or residential address or place or address of business or employment last known to the server of the notice; or

(c) sending it by post, to the person's postal or residential address or address of business or employment last known to the server of the notice, in

sufficient time for it to be delivered to that address in the ordinary course of post before the day on which the meeting is to be held; or

(d) faxing it to the member's fax number; or

(e) emailing it to the email address, of the member, that the member has nominated as the email address to which notices from the Association may be sent.

**29. Disclosure of interests**

(1) If a member of the committee or a member of a subcommittee has a direct or indirect pecuniary interest in a matter being considered, or about to be considered, by the committee or subcommittee at a meeting, the member is to, as soon as practicable after the relevant facts come to the member's knowledge, disclose the nature of the interest to the committee.

(2) If at a meeting of the committee or a subcommittee a member of the committee or subcommittee votes in respect of any matter in which the member has a direct or indirect pecuniary interest, that vote is not to be counted.

**30. Subcommittees**

(1) The committee may –
 (a) appoint a subcommittee from the committee; and

(b) prescribe the powers and functions of that subcommittee.

(2) The committee may co-opt any person as a member of a subcommittee without voting rights, whether or not the person is a member of the Association.

(3) A quorum for the transaction of the business of a meeting of the subcommittee is 3 appointed members entitled to vote.

(4) The public officer is to convene meetings of a subcommittee.

(5) Any question arising at a meeting of a subcommittee is to be determined –
 (a) on a show of hands; or

(b) if demanded by a member, by a poll taken at that meeting in the manner the chairperson determines.

(6) On any question arising at a meeting of a subcommittee, a member of the subcommittee (including the chairperson) has one vote only.

(7) Written notice of each subcommittee meeting is to be served on each member of the subcommittee by –
 (a) giving it to the member during business hours before the day on which the meeting is to be held; or

(b) leaving it, during business hours before the day on which the meeting is to be held, at the member's postal or residential address or place or address of business or employment last known to the server of the notice; or

(c) sending it by post, to the person's postal or residential address or address of business or employment last known to the server of the notice, in sufficient time for it to be delivered to that address in the ordinary course of post before the day on which the meeting is to be held; or

(d) faxing it to the member's fax number; or

(e) emailing it to the email address, of the member, that the member has nominated as the email address to which notices from the Association may be sent.

**31. Executive committee**

(1) The president, the vice-presidents, the treasurer and the secretary constitute the executive committee.

(2) During the period between meetings of the committee, the executive committee may issue instructions to the public officer and employees of the Association in matters of urgency connected with the management of the affairs of the Association.

(3) The executive committee is to report on any instructions issued under subrule (2) to the next meeting of the committee.

**32. Annual subscription**

(1) The annual subscription, for a financial year of the Association, that is payable by members of the Association is the following amount: $10

 (2) The members of the Association may alter by special resolution the annual subscription for a financial year of the Association.

(3) The annual subscription, for a financial year of the Association, that is payable by members of the Association is due and payable on the first day of the financial year.

(4) If –
 (a) a member of the Association has not paid his or her annual subscription for a financial year of the Association within 3 months after the first day of the financial year; and

(b) there has been sent to the member, after the first day of the financial year, a notice in writing, signed by the public officer, stating that the member's name may be removed from the register of members if the member has not, within 14 days after receiving the notice, paid all annual subscriptions due and payable by the member; and

(c) the member has not, within 14 days after receiving the notice, paid all annual subscriptions due and payable by the member –

the public officer may remove the name of the member from the register of members maintained under rule 5(10) .

(5) If a member of the Association has not paid his or her annual subscription for a financial year of the Association within 3 months after the first day of the financial year, or within 14 days after receiving a notice under subrule (4), whichever is the later day, he or she is not entitled to attend, or vote at, the next annual general meeting of the Association.

**33. Service of notices and requisitions**

Except as otherwise provided by these rules, a document may be served under these rules on a person by –
 (a) giving it to the person; or

(b) leaving it at, or sending it by post to, the person's postal or residential address or place or address of business or employment last known to the server of the document; or

(c) faxing it to the person's fax number; or

(d) emailing it to the person's email address.

**34. Expulsion of members**

(1) The committee may expel a member from the Association if, in the opinion of the committee, the member is guilty of conduct detrimental to the interests of the Association.

(2) The expulsion of a member under subrule (1) does not take effect until whichever of the following occurs later:
 (a) the fourteenth day after the day on which a notice is served on the member under subrule (3) ;

(b) if the member exercises his or her right of appeal under this rule, the conclusion of the special general meeting convened to hear the appeal.

(3) If the committee expels a member from the Association, the public officer, without undue delay, is to cause to be served on the member a notice in writing –
 (a) stating that the committee has expelled the member; and

(b) specifying the grounds for the expulsion; and
 (c) informing the member of the right to appeal against the expulsion under

rule 35 .
 **35. Appeal against expulsion**

(1) A member may appeal against an expulsion under rule 34 by serving on the public officer, within 14 days after the service of a notice under
 rule 34(3) , a requisition in writing demanding the convening of a special general meeting for the purpose of hearing the appeal.

(2) On receipt of a requisition, the public officer is to immediately notify the committee of the receipt.

(3) The committee is to cause a special general meeting to be held within 21 days after the day on which the requisition is received.

(4) At a special general meeting convened for the purpose of hearing an appeal under this rule –
 (a) no business other than the question of the expulsion is to be transacted; and

(b) the committee may place before the meeting details of the grounds of the expulsion and the committee's reasons for the expulsion; and

(c) the expelled member must be given an opportunity to be heard; and

(d) the members of the Association who are present are to vote by secret ballot on the question of whether the expulsion should be lifted or confirmed.

(5) If at the special general meeting a majority of the members present vote in favour of the lifting of the expulsion –
 (a) the expulsion is lifted; and

(b) the expelled member is entitled to continue as a member of the Association.

(6) If at the special general meeting a majority of the members present vote in favour of the confirmation of the expulsion –
 (a) the expulsion takes effect; and

(b) the expelled member ceases to be a member of the Association.

**36. Disputes**

(1) A dispute between a member of the Association, in his or her capacity as a member, and the Association is to be determined by arbitration in accordance with the provisions of the Commercial Arbitration Act 2011 .

(2) This rule does not affect the operation of rule 35 .

**37. Seal of Association**

(1) The seal of the Association is to be in the form of a rubber stamp inscribed with the name of the Association encircling the word "Seal".

(2) The seal is not to be affixed to any instrument except by the authority of the committee.

(3) The affixing of the seal is to be attested by the signatures of – (a) two members of the committee; or

(b) one member of the committee and – (i) the public officer; or

(ii) any other person the committee may appoint for that purpose.

(4) If a sealed instrument has been attested under subrule (3) , it is presumed, unless the contrary is shown, that the seal was affixed to that instrument by the authority of the committee.

(5) The seal is to remain in the custody of the public officer of the Association.
